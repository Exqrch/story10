from django.urls import resolve
from django.test import TestCase, Client
from django.http import HttpRequest
from .views import *
# Create your tests here.
class UnitTest(TestCase):
	def test_app_url_exist(self):
		response=Client().get('')
		self.assertEqual(response.status_code,200)